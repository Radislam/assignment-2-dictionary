APPNAME=app

%.o: %.asm
	nasm -f elf64 -o $@ $< -g


.PHONY: build

build: lib.o dict.o main.o
	ld -o $(APPNAME) lib.o dict.o main.o
	rm *.o

	
.PHONY: clean

clean:
	rm -rf *.o
	rm -rf $(APPNAME)

.PHONY: run

run: build
	./$(APPNAME)

.PHONY: test

test: build
	python3 test.py
