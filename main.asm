%include "lib.inc"
%include "dict.inc"
%include "words.inc"

%define buffer_size 256


section .rodata
msg_err: db "Error!", `\n`, 0

section .bss
input_buffer: resb buffer_size

section .text

%define pointer_size 8
global _start

_start:
    ; Чтение слова
    mov rdi, input_buffer 
    mov rsi, buffer_size - 1
    call read_word
    test rax, rax 
    jz .error

    ; Поиск слова
    mov rsi, third
    mov rdi, input_buffer
    call find_word
    test rax, rax
    jz .error

    ; Обработка найденного слова
    mov rdi, rax
    add rdi, pointer_size
    push rdi
    call string_length
    pop rdi
    add rdi, rax
    inc rdi
    call print_string
    xor rdi, rdi
    call exit

    ; Обработка ошибки
    .error: 
        mov rdi, msg_err
        call print_error
        call exit
